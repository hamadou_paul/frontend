import React, { useState } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import API from "../../utils/API";
import setAuthToken from "../../utils/setAuthToken";
import { useDispatch } from "react-redux";
import { setAuthentificated, setToken, setUser } from "../../store/user/UserSlice";
import "./Login.styles.css";

import { NavLink, useHistory, useNavigate } from "react-router-dom";
import axios from "axios";

const loginSchema = Yup.object().shape({
  email: Yup.string()
    .email("Veillez saisir une adresse mail valide")
    .required("Veillez saisir le nom de l'utilisateur"),
  password: Yup.string()
    .required("Veillez saisir votre mot de passe."),
});

const Login = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [messageError,setMessageError] = useState("");

  const handleSubmit = (values, { setSubmitting }) => {

    API.post("/api/login",values)
    .then((resp) => {
      setAuthToken(resp.data.token);
      dispatch(setToken(resp.data.token));
      API.get('/api/me')
      .then((response) => {
        dispatch(setUser(response.data));
      })
      setTimeout(dispatch(setAuthentificated(true)),1000);
      navigate("/utilisateurs");
    }).catch((error) => {
      setMessageError("email ou mot de passe invalide.");
      setSubmitting(false);
    });

    
    
  };

    return (
      <div className="dd-login-container row mt-auto">
        
        <div className="dd-login-left col-md-6 pr-5 pl-5 mt-50">
          <div className="dd-login-welcome">
            Se connecter 
          </div>
          
          <div className="dd-login-form">
            <Formik
            initialValues={{email:"",password:""}}
              validationSchema={loginSchema}
              onSubmit={handleSubmit}
            >
              {({ isSubmitting,setFieldValue }) => {
                return (
                  <Form>
                    <span className="label-error backend">{messageError}</span>
                    <div>
                      <label>Nom d'utilisateur:</label>
                      <Field type="text" name="email" onChange={(e) => {
                        setMessageError(" ");
                        setFieldValue("email",e.target.value);
                      }} />
                      <div className="dd-input-error"> <ErrorMessage name="email" component="div" className="label-error" /></div>
                    </div>
                    <div>
                      <label>Mot de passe:</label>
                      <Field type="password" name="password" onChange={(e) => {
                        setMessageError(" ");
                        setFieldValue("password",e.target.value);
                      }}/>
                      <div className="dd-input-error"><ErrorMessage name="password" component="div" className="label-error"  /></div>
                    </div>
                    <button type="submit" className={isSubmitting ? "disabled mb-2":"mb-2"} disabled={isSubmitting}>
                      CONNEXION
                    </button>

                    Vous n'avez de compte? <NavLink to={"/register"}>S'incrire</NavLink>

                  </Form>
                );
              }}
            </Formik>
          </div>
        
        </div>
        <div className="dd-login-rigth d-none d-md-block col-md-6">
        </div>
        
      </div>
    );
}

export default Login;
