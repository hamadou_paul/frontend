import React from "react";
import "./Page404.styles.css";
import img from "../../assets/img/admin.png";
import { Link } from "react-router-dom";
import { AiOutlineArrowLeft } from "react-icons/ai";


const Page401 = () => {
  return (
    <div className="min-h-full flex items-center justify-center py-12 px-4 sm:px-6 lg:px-8 page-401">
      <div className="text-404">
        <div className="h404">401</div>
        <div className="text">Not Authorized</div>
        <img className="image-404" src={img} />
        <div>Vous n'est pa habilité à consulter cette page.</div>
      </div>
    </div>
  );
};

export default Page401;
