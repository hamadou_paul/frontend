import React from "react";
import "./Page404.styles.css";
import { Link } from "react-router-dom";
import { AiOutlineArrowLeft } from "react-icons/ai";

const Page404 = ({home = "/produits"}) => {
  return (
    <div className="min-h-full flex items-center justify-center py-12 px-4 sm:px-6 lg:px-8 not_found_bg">
      <div className="text-404">
        <div className="h404">404</div>
        <div className="text">Page Not Found</div>
        <div>Oops, it seems you follow backlink</div>
        <Link to={home} className="goto-home btn"> <AiOutlineArrowLeft /> Back To Home</Link>
      </div>
      
    </div>
  );
};

export default Page404;
