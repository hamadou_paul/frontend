import React, { useState } from "react";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import API from "../../utils/API";
import "../Login/Login.styles.css";

import { NavLink, useNavigate } from "react-router-dom";
import FormField from "../../components/Form/FormField";


const registerSchema = Yup.object().shape({
  name: Yup.string()
  .required("Veillez saisir le nom de l'utilisateur"),
  email: Yup.string()
    .email("Veillez saisir une adresse mail valide")
    .required("Veillez saisir le nom de l'utilisateur"),
  password: Yup.string()
    .min(8,'Mot de passe trop court.')
    .required('Mot de passe obligatoire'),
  passwordConfirmation: Yup.string()
      .oneOf([Yup.ref('password'), null], 'Les mots de passe doivent correspondre.')

});



const Register = () => {
  const navigate = useNavigate();
  const [messageError,setMessageError] = useState("");

  const handleSubmit = (values, { setSubmitting }) => {
    API.post("/api/register",values)
    .then((resp) => {    
      navigate("/login");
    }).catch((error) => {
      setMessageError("Nous n'avons pas pu créer votre compte.");
      setSubmitting(false);
    });  
  };

    return (
      <div className="dd-login-container row">

        <div className="dd-login-left col-md-6 pr-5 pl-5">
          <div className="dd-login-welcome">
            Register
          </div>
          
          <div className="dd-login-form">
            <Formik
            initialValues={{name:"",email:"",password:"",passwordConfirmation:""}}
              validationSchema={registerSchema}
              onSubmit={handleSubmit}
            >
              {({ isSubmitting,setFieldValue,errors,values }) => {
                return (
                  <Form>
                    <span className="label-error backend">{messageError}</span>
                    
                    <FormField name="name" label={"Nom"} errors={errors} required />
                    
                    <FormField name="email" label={"E-mail"} errors={errors} required />

                    <FormField type='password' name="password" label={"Mot de passe"} errors={errors} required />
                    <FormField type='password' name="passwordConfirmation" label={"Confirmation du mot de passe"} errors={errors} required />

                    <button type="submit" className={isSubmitting ? "disabled mb-2":"mb-2"} disabled={isSubmitting}>
                      S'INSCRIRE
                    </button>
                 
                    Vous avez déjà un compte? <NavLink to={"/login"}>Se connecter</NavLink>
                  </Form>
                );
              }}
            </Formik>
          </div>
        </div>
        <div className="dd-login-rigth d-none d-md-block col-md-6">
        </div>
        
      </div>
    );
}

export default Register;
