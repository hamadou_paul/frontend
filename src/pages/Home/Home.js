import React from "react";
import { NavLink } from "react-router-dom";

const Home = () => {
  return (
    <div className="">
      Bienvenue

      <div className="d-flex">
        <NavLink to={"/login"} className="btn btn-primary ml-auto mr-5 " >Se connecter</NavLink>
        <NavLink to={"/register"} className="btn btn-secondary ">S'inscrire</NavLink>
      </div>
    </div>
  );
};

export default Home;
