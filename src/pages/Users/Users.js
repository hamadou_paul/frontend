import React,{useContext, useEffect, useState} from "react";
import { setPaginateBaseUrl, setTextAide,setTextRecherche } from "../../store/page/PageSlice";
import { useDispatch } from "react-redux";
import PageTitle from "../../components/PageTitle/PageTitle";
import { FaUserFriends } from "react-icons/fa";
import Table from "../../components/Table/Table";
import { MdGroupAdd } from "react-icons/md";
import FormDialog from "../../components/Form/FormDialog";
import { AbilityContext } from "../../security/Can";

const Users = () => {
  const dispatch = useDispatch();
  const [visible,setVisible] = useState(false);
  const ability = useContext(AbilityContext);

  const actions = [
    
  ];

  const [columns,setColumns] = useState([
    {
      name: 'Nom',
      field: 'name',
    },
    {
      name: 'E-mail',
      field: 'email',
    }
  ]);
  


  useEffect(() => {
    dispatch(setTextRecherche("Rechercher un utilisateur..."));
    dispatch(setPaginateBaseUrl("users"));
  },[]);

  return (
    <div className="">
      <PageTitle 
        icon={<FaUserFriends />}
        title={"Liste des simples utilisateurs"}
        actions={actions}
      /> 
      <Table columns={columns} baseUrl={"/api/users"} />
    </div>
  );
};

export default Users;
