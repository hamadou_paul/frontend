import React,{useEffect, useState} from "react";
import { selectCartStatus, setCartStatus, setPaginateBaseUrl, setTextAide,setTextRecherche } from "../../store/page/PageSlice";
import { useDispatch , useSelector} from "react-redux";
import PageTitle from "../../components/PageTitle/PageTitle";
import { FaBox } from "react-icons/fa";
import { IoMdAdd, IoMdCart } from "react-icons/io";
import { TiDocumentAdd } from "react-icons/ti";
import Table from "../../components/Table/Table";
import FormProduit from "./FormProduit";
import "./Produits.styles.css";
import FormDialog from "../../components/Form/FormDialog";

const Produits = () => {
  const dispatch = useDispatch();
  const [visible,setVisible] = useState(false);
  const [visibleImport,setVisibleImport] = useState(false);

  const cartStatus = useSelector(selectCartStatus);

  const actions = [
    {
      action: "create",
      entity: "Product",
      icon: <IoMdAdd />,
      title: "Nouveau produit",
      onClick: () => setVisible(true),
    }
  ];

  const [columns,setColumns] = useState([
    {
      name: 'Ref',
      sortable: true,
      field: "reference",
    },
    {
      name: 'Libellé du produit',
      field: 'imageName',
    },
    {
      name: 'Catégorie du produit',
      field: 'category',
    },
    {
      name: 'Qté Disponible',
      field: 'quantity',
    },
    {
      name: 'Qté Expirée',
      field: 'expired',
    },
    {
      name: 'Status',
      field: 'state'
    },
    {
      name: 'Action',
      field: 'action'
    }
  ]);

  useEffect(() => {
    dispatch(setTextRecherche("Rechercher un produit..."));
    dispatch(setPaginateBaseUrl("products"));
  },[]);

  useEffect(() => {
    if(cartStatus == "open"){
      setColumns([
        {
          name: 'Libellé du produit',
          field: 'imageName',
        },
        {
          name: 'Qté Disponible',
          field: 'quantity',
        },
        {
          name: 'Qté Expirée',
          field: 'expired',
        },
        {
          name: 'Action',
          field: 'action'
        }
      ]);
    }else{
      setColumns([
        {
          name: 'Ref',
          sortable: true,
          field: "reference",
        },
        {
          name: 'Libellé du produit',
          field: 'imageName',
        },
        {
          name: 'Catégorie du produit',
          field: 'category',
        },
        {
          name: 'Qté Disponible',
          field: 'quantity',
        },
        {
          name: 'Qté Expirée',
          field: 'expired',
        },
        {
          name: 'Status',
          field: 'state'
        },
        {
          name: 'Action',
          field: 'action'
        }
      ]);
    }
  },[cartStatus]);



  return (
    <div className="">
      
      <PageTitle 
        icon={<FaBox />}
        title={"Liste des produits"}
        actions={actions}
      /> 
      <Table columns={columns} baseUrl={"/api/products"}/>
      <FormDialog visible={visible} setVisible={setVisible} baseUrl={"products"}/>
    </div>
   
  );
};

export default Produits;

