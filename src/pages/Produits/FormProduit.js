import { Formik, Form } from 'formik';
import * as Yup from 'yup';

import FormField from '../../components/Form/FormField';

const Schema = Yup.object().shape({
  
  titre: Yup.string()
    .required('Veillez saisir le titre du produit'),
});


const FormProduit = ({dispatch,fake,item = {
    titre: "",
    description: '',
    image: null,
  },handleSubmit}) => {

  return(
    <Formik
      initialValues={item}
      validationSchema={Schema}
      onSubmit={(value) => {
        let formData = new FormData();
        formData.append("image",value.image);
        formData.append("titre",value.titre);
        formData.append("description",value.description);
        handleSubmit("products",formData,() => {})}
      }
    >
      {({ errors,isSubmitting,values,handleChange,handleBlur,initialValues,setFieldValue }) => {
        return (
          <Form >
            {isSubmitting && <div className="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>}
            <div className="invf-form-body">
              <FormField name="titre" label={"Libellé du produit"} errors={errors}  required />
              <FormField name="description" label={"Description du produit"}  errors={errors} />
              
              <input accept='image/*' id='upload_file' type='file' name="image" label={"Fichier des produits"}  onChange={(e) => {
                setFieldValue("image",e.target.files[0]);
              }} />
              <div className="invf-form-submit">
                <button 
                  disabled={isSubmitting}
                  type="submit" className="btn btn-dark w-100" >
                  ENREGISTRER
                </button>
              </div>
            </div>
          </Form>
        );
      }}
    </Formik>
  );
}


export default FormProduit;