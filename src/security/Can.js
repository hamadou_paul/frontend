import { createContext } from 'react';
import { createContextualCan } from '@casl/react';

export const AbilityContext = createContext();
export const Can = createContextualCan(AbilityContext.Consumer);


export function unauthorized(keycloak) {
  if( keycloak && keycloak.authenticated) {
    return true;
  }else{
    return false;
  }
}