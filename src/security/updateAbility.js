import { AbilityBuilder, Ability } from '@casl/ability';

export function updateAbility(ability, fake) {
  const { can, rules } = new AbilityBuilder(Ability);
  
  can('manage', 'all');
  
  ability.update(rules);
}