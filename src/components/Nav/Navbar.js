import React from "react";
import "./Navbar.styles.css";
import { logout } from "../../store/user/UserSlice";
import { useDispatch } from "react-redux";

import { NavLink, useNavigate } from "react-router-dom";
import { Accordion } from '@mantine/core';
import routes from "../../routes";
import classNames from "classnames";

const Navbar = () => {
  const dispatch = useDispatch();
  let navigate = useNavigate();

  return (
    <>
      <div className="invf-nav-logo" to="/" onClick={() => {
          navigate("/");
        }}>
        <div className="inv-nav-logo-img"></div>
      </div> 
      <div className="invf-nav-menus invf-tiny-scroll">
        <Accordion defaultValue="customization" >
          {routes.map((item) => (
            <div key={item.to}>
              {item.children ? 
                <>
                  <Accordion.Item value={item.to}>
                    <Accordion.Control className="invf-nav-menu-btn" /*chevron={<>{item.children && <BiChevronDown />}</>}*/> 
                      <span className="icon"><item.icon /></span><span className="title">{item.name}</span>
                    </Accordion.Control>
                      <Accordion.Panel>
                        {item.children.map((child) => (
                          <NavLink
                          key={child.to}
                          to={item.to + child.to}
                          className={({ isActive }) => classNames(
                            {" active text-white dark:bg-slate-900 dark:text-white":isActive},
                            {"text-slate-900 hover:bg-slate-200 font-thin text-white dark:hover:bg-slate-700 dark:hover:bg-opacity-75 opacity-50 cursor:pointer":!isActive},
                            "invf-nav-menu group flex flex-wrap items-center p-2 text-base font-medium relative"
                          )}
                        >{child.name}</NavLink>
                        ))}
                      </Accordion.Panel>
                  </Accordion.Item>
                </>:
                <>
                <NavLink
                    key={item.to}
                    to={item.to}
                    className={({ isActive }) => classNames(
                      {"mantine-Accordion-control link active text-white dark:bg-slate-900 dark:text-white":isActive},
                      {"mantine-Accordion-control link text-slate-900 hover:bg-slate-200 font-thin text-white dark:hover:bg-slate-700 dark:hover:bg-opacity-75":!isActive},
                      "invf-nav-menu group flex flex-wrap items-center p-2 text-base font-medium relative"
                    )}
                  ><span className="icon"><item.icon /></span><span className="title">{item.name}</span></NavLink>
                </>}
            </div>
          ))}
        </Accordion>    
      </div>
      
      <div className="invf-nav-logout" onClick={() => dispatch(logout()) }>
        
        Log Out
      </div>
    </>
  );
};

export default Navbar;
