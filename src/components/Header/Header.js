import React, { useState } from "react";
import "./Header.styles.css";
import { BiSearchAlt } from "react-icons/bi";
import { IoMdNotifications } from "react-icons/io";
import { selectTextRecherche, setSearch } from "../../store/page/PageSlice";
import {  useDispatch, useSelector } from "react-redux";
import { logout, selectCurrentUser } from "../../store/user/UserSlice";
import { Menu, MenuButton, MenuHeader } from "@szhsin/react-menu";
import {IoMdArrowDropdown} from "react-icons/io";
import {RiLogoutCircleRLine} from "react-icons/ri";

import defaultAvatar from "../../assets/img/bg-avatar.png";
import {MdOutlineModeEdit} from "react-icons/md";
import { selectSearch } from './../../store/page/PageSlice';




const Header = () => {
  const textRecherche = useSelector(selectTextRecherche);
  const currentUser = useSelector(selectCurrentUser);
  const dispatch = useDispatch();
  const search = useSelector(selectSearch);

  const [avatar, setAvatar] = useState(defaultAvatar);

  return (
    <div className="invf-header">
      <div className="invf-header-search">
        <input 
          name="search" 
          id="search" 
          type={"text"}  
          placeholder={textRecherche}
          value={search}
          onChange={(e) => dispatch(setSearch(e.target.value))}
        />
        <BiSearchAlt />
      </div>
      <div className="invf-header-notification">
      </div>
      <div>
        <Menu 
          direction={"bottom"}
          arrow={false}
          className={"invf-menu-profile"}
          offsetY={8}
          offsetX={25}
          
          menuStyle={{backgroundImage:'url('+avatar+')'}}
          menuButton={
          <MenuButton className={"menu-profile invf-header-acount"}>
            
            <img src={avatar} className="fa-user"/>
            <span className="text-ellipsis">{currentUser?.name}</span>
          </MenuButton>}
        >
        </Menu>
      
      </div>
    </div>
  );
};

export default Header;
