import React from "react";
import "./PageTitle.styles.css";
import { useDispatch, useSelector } from "react-redux";
import { selectCartStatusControl } from "../../store/page/PageSlice";
import { Can } from "../../security/Can";
import Tippy from '@tippyjs/react';
import 'tippy.js/dist/tippy.css'; // optional

const PageTitle = ({icon,title,actions}) => {
  const dispatch = useDispatch();
  const cartStatusControl = useSelector(selectCartStatusControl);


  return (
    <div className="invf-page-title">
      <div className="invf-page-title-icon">
        {icon}
      </div>
      <div className="invf-page-title-text">
        {title}
      </div>
      <div className="invf-page-title-btns">
        {actions.map((action) => (
          <Can I={action.action} a={action.entity} key={action.title}>
            <Tippy 
            
            offset={[0,10]}
            placement={"top"} 
            content={<span>{action.title}</span>} 
            arrow={false}
            disabled={cartStatusControl == "close"}
          >
            <button 
              
              className={"invf-page-title-btn " + cartStatusControl}
              onClick={() => action.onClick(dispatch,null)}
            >
              {action.icon}
              <span>{cartStatusControl == "close" && action.title}</span>
            </button>
          </Tippy>
            
          </Can>
        ))}   
      </div>
    </div>
  );
};

export default PageTitle;
