import { Field } from "formik"
import classNames from "classnames";
import {BiError} from "react-icons/bi";
import { Calendar } from "primereact/calendar";



const FormField = ({name,label,errors,className,required,type="text",values,setFieldValue}) => {
  return (
    <div className={"invf-form-field w-100 "+className}>
      <label className="form-label " htmlFor={name} >{label} {required && <span className=" text-danger">*</span>}:</label>
      <div className={classNames({"invf-form-field-is-invalid":errors[name]})}>
        {type == "date" ? 
          <Calendar inputClassName={classNames("form-control",{"invf-is-invalid":errors[name]})} value={values[name]} onChange={(e) => setFieldValue(name,e.value||"")}></Calendar>
          :
          <Field type={type} name={name} id={name} className={classNames("form-control",{"invf-is-invalid":errors[name]})} />}
        {errors[name]  && <div className="span  text-danger"> <BiError/>{errors[name]}</div>}
      </div>
    </div>
  )
}

export default FormField;