import classNames from "classnames";
import makeAnimated from "react-select/animated";
import { ROOT_V1 } from "../../config/constantes";
import API from "../../utils/API";
import { useEffect, useState } from "react";
import Select from 'react-select';
import { BiError } from "react-icons/bi";
import axios from "axios";

const animatedComponents = makeAnimated();

const FormSelect = ({isMulti,name,label,errors,className,required,url,values,setFieldValue,options}) => {
  const [_options,setOptions] = useState([]);

  const [selectLabel,setLabel] = useState("Select ...");

  useEffect(()=>{
    if(values && values[name]){
      console.log(values[name].reference)
      if(values[name].name){
        setLabel(values[name].name);
      }else if(values[name].reference){
        setLabel(values[name].reference);
      }else if(values[name].id){
        //setLabel(values[name].id);
      }else {
        setLabel(values[name]);
      }
    }
    
  },[values]);

  useEffect(()=>{
    url && axios.get(url)
    .then((response) => {
      let items = [];
      response?.data?.data.map((data) => {
        if( url.indexOf("commands") >= 0){
          items.push({
            label:data.name?data.name:data.reference,
            value: data
          })
        }else{
          items.push({
            label:data.name?data.name:data.reference,
            value:{
              id: data.id,
              name: data.name?data.name:data.reference
            }
          })
        }
        
      })
      setOptions(items); 
    });

    options && setOptions(options);
  },[]);


  return (
    <div className={"invf-form-field "+className}>
      <label className="form-label " htmlFor={name} >{label} {required && <span className=" text-danger">*</span>}:</label>
      <div className={classNames({"invf-form-field-is-invalid":errors[name]})}>
        <Select
          //isMulti={isMulti}
          name={name}
          cacheOptions
          value={{
            label: selectLabel,
            value: values[name]
          }}
          components={animatedComponents} 
          onChange={(option) => {  isMulti? setFieldValue(name,[option.value]):setFieldValue(name,option.value) }}
          options={_options}
          className={classNames("form-control-react-select",{"is-invalid":errors[name]})} 
        />
        {errors[name]  && <div className="span  text-danger"> <BiError/>{errors[name]}</div>}
      </div>
    </div>
  )
}

export default FormSelect;