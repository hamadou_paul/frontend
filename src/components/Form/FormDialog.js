
import { Dialog } from 'primereact/dialog';
import { ROOT_V1 } from '../../config/constantes';
import API from '../../utils/API';
import { useDispatch, useSelector } from 'react-redux';
import React from 'react';
import { addOrModifyDataTable, setAlerteDialog } from '../../store/page/PageSlice';
import { FaTimes } from 'react-icons/fa';
import "./FormDialog.styles.css";
import items from '../../utils/items';
import axios from 'axios';
import { selectToken } from '../../store/user/UserSlice';

const FormDialog = ({visible,setVisible,item = {},baseUrl}) => {
  
   const dispatch = useDispatch();

   //const baseUrl = useSelector(selectPaginateBaseUrl);
   const name = items[baseUrl].define_articleName;
   const icon = items[baseUrl].icon;
   const WrappedComponent = items[baseUrl].form;
   const token = useSelector(selectToken);

  const handleSubmit = (url,item,addOrModifyItem) => {

    const config = {
      headers: { Authorization: `Bearer ${token}` }
    };

    var request ;

    if(item.id){
      request = API.put(url+"/"+item.id,item,config);
    }else{
      request = API.put(url,item,config);
    }
    request.then((response) => {
      setVisible(false);
      dispatch(addOrModifyDataTable(response.data));
      dispatch(setAlerteDialog({
        visible:true,
        type: "success",
        title: "Modification éffectuée avec succès.",
        description: 'response.data.message'
      }));
    })
    .catch((error) => {
      setVisible(false);
      dispatch(setAlerteDialog({
        visible:true,
        type: "error",
        title: "Echec de modification.",
        description: 'error.response.data.message'
      }));
    });
    
  }

    return (
      <>
        <Dialog 
          visible={visible} 
          draggable={false}
          closable={false}
          className={"invf-main-form"}
          headerClassName={"invf-form-header"}
          contentClassName={"invf-form invf-tiny-scroll"}
          header={
            <div className='invf-form-header-container'>
              <div className='invf-form-header-icon'>{icon} </div>
              <div className='invf-form-header-title'>{item.id ? "Modifier " + name: "AJouter "+ name}</div>
              <div className='invf-form-header-close' onClick={() => setVisible(false)}><FaTimes/> </div>
            </div>
          }
          footer={<div className='invf-form-footer'></div>}
          onHide={() => setVisible(false)}
        >
          <WrappedComponent  handleSubmit={handleSubmit} item={item} /> 
        </Dialog>
      </>
    )
}



export default FormDialog;