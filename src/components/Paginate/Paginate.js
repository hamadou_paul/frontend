import React,{useEffect, useState} from "react";
import "./Paginate.styles.css";
import { useDispatch, useSelector } from "react-redux";
import ReactPaginate from 'react-paginate';
import { getAsyncDataTable, selectPaginate } from "../../store/page/PageSlice";
import { selectSearch } from './../../store/page/PageSlice';
import { selectToken } from "../../store/user/UserSlice";

const Paginate = ({baseUrl}) => {

  const dispatch = useDispatch();
  
  const [page,setPage] = useState(1);
  const search = useSelector(selectSearch);
  const token = useSelector(selectToken);

  const pageData  = useSelector(selectPaginate);// default page size

  const handleChangePage = (page) => {
    setPage(page);
    let url =  baseUrl + "?page=" + page + "&limit=" + pageData.sizePage;
    //if(search) url += "?search=" + search;
   
    dispatch(getAsyncDataTable( url ));
    //load the new page here
  }

  useEffect(() => {
    //handleChangePage(1);//initialization
  },[]);

  useEffect(() => {
    handleChangePage(page);
  },[baseUrl,search]);

  return (
    <div className="invf-paginate">
      <div className="invf-paginate-text">
        Page {page} sur {pageData.countPage}
      </div>
      <div className="invf-paginate-btns">
        <ReactPaginate
          breakLabel="..."
          nextLabel="Suivant"
          onPageChange={(event) => handleChangePage(event.selected +1 )}
          pageRangeDisplayed={5}
          pageCount={pageData.countPage}
          previousLabel="Précédent"
          renderOnZeroPageCount={null}
          pageClassName="page-item"
          marginPagesDisplayed={1}
          nextClassName="invf-nav-arrow"
          previousClassName="invf-nav-arrow"
          activeClassName="active"
        />
      </div>
    </div>
  );
};

export default Paginate;
