
import { Dialog } from 'primereact/dialog';
import React from 'react';
import { FaTimes } from 'react-icons/fa';
import { useDispatch, useSelector } from 'react-redux';
import { selectAlerteDialog, setAlerteDialog } from '../../store/page/PageSlice';
import "./AlerteDialog.styles.css";


const AlerteDialog = () => {
  const dispatch = useDispatch();
  const alerte = useSelector(selectAlerteDialog);
  
  return (
    <Dialog 
      width={420}
      
      visible={alerte.visible} 
      resizable={false}
      draggable={false}
      closable={false}
      className={"invf-alert"}
      style={{ width: '50vw' }} 
      headerClassName={"invf-alerte-header"}
      contentClassName={"invf-alert-content"}
      header={<div className='invf-alerte-header-close' onClick={() => dispatch(setAlerteDialog({visible:false}))}><FaTimes/> </div>}
      footer={<div className='invf-alerte-footer'></div>}
      onHide={() => dispatch(setAlerteDialog({visible:false}))}
    >
      <div className={'invf-alert-img '+alerte.type}></div>
      <div className={'invf-alert-title '+alerte.type}>{alerte.title}</div>
      <div className={'invf-alert-description '+alerte.type}>{alerte.description}</div>
    </Dialog>
  )
}



export default AlerteDialog;