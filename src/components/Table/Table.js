import { useEffect, useState } from 'react';

import { useSelector,useDispatch } from "react-redux";
import {  selectPageStatus } from '../../store/page/PageSlice';
import { selectDataTable } from '../../store/page/PageSlice';

import Action from "../Action/Action";
import Paginate from '../Paginate/Paginate';
import TableLoader from '../Loader/TableLoader';
import classNames from "classnames";
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import "./Table.styles.css";
import moment from 'moment';

const  Table = ({baseUrl,columns}) => {
  const dispatch = useDispatch();

  const status = useSelector(selectPageStatus);
  const data = useSelector(selectDataTable);
  const [_columns,setColumns] = useState([]);
  useEffect(() => {
    let _cols = [];
    columns && columns.map((col) => {
    let _col = Object.assign({},col);
    // cette section du code permet de customizer l'affichage de certain champs du tableau
    if(col.field == "action"){
      _col.cell = (row, index, column, id)  => {
        return <Action item={row} baseUrl={baseUrl} Element={col.element}/>;
      };
    }else if(["createdAt","expiry",'createdDate'].indexOf(col.field) >= 0){
      _col.cell = (row, index, column, id)  => {
        return <>{moment(row[col.field]).format('YYYY-MM-DD HH:mm')}</>;
      };
    }
      _col.resizeable = true;
      _cols.push(_col);
    });
    setColumns(_cols);
  },[columns]);

  useEffect(() => {

  },[data]);
    return (
      <>
        {status == 'loading'?
          <TableLoader />:
          <>
            <DataTable 
              value={data} 
              responsiveLayout="scroll"
              stripedRows 
              className='rdt_Table invf-tiny-scroll'
              rowClassName='rdt_TableRow'
              cellClassName='rdt_TableCell'
              rowHover               
            >
              {_columns.map((col,idx) => (
                <Column 
                  headerClassName='rdt_TableCol'
                  key={col.field}
                  field={col.field} 
                  header={col.name} 
                  body={(rowData) => 
                    <>
                      {col.cell?col.cell(rowData, idx, col, rowData.id):rowData[col.field]}
                    </>
                  }
                  ></Column>
              ))}
            </DataTable>
          </>
          
        }
        <Paginate baseUrl={baseUrl} />
      </>
    );
};

export default Table;