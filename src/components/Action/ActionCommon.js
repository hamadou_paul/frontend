import { MenuItem } from "@szhsin/react-menu";
import { useSelector } from "react-redux";
import "./Action.styles.css";
import {  selectPaginateBaseUrl } from "../../store/page/PageSlice";

const ActionCommon = ({item,baseUrl,setVisible}) => {
  
  const url = useSelector(selectPaginateBaseUrl);

  return(
    <>
      <MenuItem onClick={() => setVisible(true)}><div>Modifier</div></MenuItem>
    </>
  );
} 

export default ActionCommon;