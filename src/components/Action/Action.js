import { Menu, MenuButton, MenuItem } from "@szhsin/react-menu";
import { HiDotsVertical } from "react-icons/hi";
import { useSelector } from "react-redux";
import "./Action.styles.css";
import {  selectPaginateBaseUrl } from "../../store/page/PageSlice";
import FormDialog from "../Form/FormDialog";
import { useState } from "react";

import items from "../../utils/items";

const Action = ({item,baseUrl,Element}) => {
  const url = useSelector(selectPaginateBaseUrl);
  const [visible,setVisible] = useState(false);


  return(
    <>

      <Menu
        align={"start"} 
        position={"anchor"} 
        direction={'right'}
        offsetX={0}
        offsetY={-12}      
        className={"invf-react-menu"} 
        menuButton={<MenuButton className={"invf-react-menu-button"}><HiDotsVertical /></MenuButton>}>
          <Element item={item} setVisible={setVisible} baseUrl={baseUrl}/>
      </Menu>
      {items[url] && <FormDialog visible={visible} setVisible={setVisible} item={item} baseUrl={url}/>}    
    </>

  );
} 

export default Action;