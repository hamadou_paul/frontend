export const ROOT_V1 = "";//"/api/v1";
export const keycloak_client = "infirmerie";
export const keycloak_url = "https://idpo.orange.cm";
export const keycloak_realm = "digital-app";

export const roles = [
    // that users roles does'nt have acces to backoffices
    // {label:"Utilisateur",value:"ROLE_USER"},
    // {label:"Passager",value:"ROLE_PASSAGER"},
    // {label:"Driver",value:"ROLE_DRIVER"},
    {label:"Gestionaire des profiles",value:"ROLE_PROFILER"},
    {label:"Gestionaire des comptes",value:"ROLE_COMPTABLE"}
];

export const sexes = [
    // that users roles does'nt have acces to backoffices
    // {label:"Utilisateur",value:"ROLE_USER"},
    // {label:"Passager",value:"ROLE_PASSAGER"},
    // {label:"Driver",value:"ROLE_DRIVER"},
    {label:"Maxculin",value:"Maxculin"},
    {label:"Feminin",value:"Feminin"}
];
  
  
