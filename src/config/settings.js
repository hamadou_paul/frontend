const settings = {
  baseUrl: window.env.BASE_URL,
  keycloak_client : window.env.CLIENT_KEYCLOAK,
  keycloak_url : window.env.URL_KEYCLOAK,
  keycloak_realm : window.env.REALM_KEYCLOAK
};

export default settings;