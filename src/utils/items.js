import { BiCategory } from "react-icons/bi";
import { FaBox,FaBoxOpen,FaUsersCog, FaUserFriends, FaHandHoldingMedical} from "react-icons/fa";
import FormProduit from "../pages/Produits/FormProduit";

const items = 
  {
    products: {
      icon: <BiCategory />,
      form: FormProduit, 
      name:"Produit ",
      define_articleName: "un produit",
      undefine_articleName: "le produit "
    }
  };

export default items;