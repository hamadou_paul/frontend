import axios from "axios";
import { toast } from "react-toastify";
import settings from './../config/settings';

const API = axios.create({
  baseURL: settings.baseUrl,
  headers: { 
    "Referrer-Policy" : "no-referrer",
  },
});

API.interceptors.response.use(function (response) {
    return response;
  }, function (error) {
    displayError(error);
    return Promise.reject(error);
  });

export const error500Message = "Une erreur est survenue !";

export const displayError = (error) => {
   
    if(error.response && error.response.status && error.response.status >= 400 && error.response.status < 500) {
        toast.error(error.response.data.detail);
    } else {
        toast.error(error500Message);
    }
};

export default API;
