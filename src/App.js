import React,{useEffect,useContext, Fragment } from 'react';
import "primereact/resources/themes/lara-light-indigo/theme.css";  //theme
import "primereact/resources/primereact.min.css";                  //core css

import '@szhsin/react-menu/dist/index.css';
import '@szhsin/react-menu/dist/transitions/slide.css';
import { setUser,setAuthentificated,selectCurrentUser, selectToken, selectAuthentificated } from './store/user/UserSlice';
import { useDispatch, useSelector } from "react-redux";
import setAuthToken from './utils/setAuthToken';
import 'bootstrap/dist/css/bootstrap.min.css';
import "flatpickr/dist/flatpickr.css";

import Navbar from './components/Nav/Navbar';
import {
  Routes,
  Route,
  useLocation,
} from "react-router-dom";

import Hearder from './components/Header/Header';
import Produits from './pages/Produits/Produits';
import AlerteDialog from './components/Alerte/AlerteDialog';

import { Can, AbilityContext, unauthorized  } from './security/Can';
import { ToastContainer } from 'react-toastify';
import Login from './pages/Login/Login';


import './App.css';
import Page404 from './pages/Errors/Page404';
import Page401 from './pages/Errors/Page401';
import routes from './routes';
import Home from './pages/Home/Home';
import Register from './pages/Register/Register';



function App() {

  const token = useSelector(selectToken);
  const authenticated = useSelector(selectAuthentificated);
  const dispatch = useDispatch();
  useEffect(() => {
        setAuthToken(token);
    },[token]);

  const ability = useContext(AbilityContext);
  const currentUser = useSelector(selectCurrentUser);
  useEffect(() => {
    dispatch(setUser(null));
  },[]);

  return (
    <>
    <div className="App">
      {authenticated == true ?  
        <React.StrictMode>
          <div className='invf-main'>
            <div className='invf-nav'>
                <Navbar />
            </div>
            <div className='invf-body'>
              
              <Hearder />
              <div className='invf-body-container'>
                <div className={'invf-main-body expand' }>
                  <Routes>
                    {routes.map((route) => (
                      <Route key={route.to} path={route.to} element={<route.element /> }></Route>
                    ))}
                    <Route  path={"*"} element={<Page404 /> }></Route>
                  </Routes>
                </div>
              </div>
            </div>
          </div>
        </React.StrictMode>
        : 
        <div className='invf-main'>
          <Routes>
            <Route  path={"/"} element={<Home /> }></Route>
            <Route  path={"/login"} element={<Login /> }></Route>
            <Route  path={"register"} element={<Register /> }></Route>
            <Route  path={"*"} element={<Page404  home="/"/> }></Route>
          </Routes>
        </div>
      }
      <AlerteDialog />
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={true}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
    />
    </div>
  </>
  );
}

export default App;
