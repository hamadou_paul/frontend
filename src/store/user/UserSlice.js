import { createSlice } from "@reduxjs/toolkit";
import setAuthToken from "../../utils/setAuthToken";

export const UserSlice = createSlice({
  name: "user",
  initialState: {
    user: null,
    authentificated: false,
    token: null
  },
  reducers: {
    setToken: (state,action) => {
      state.token = action.payload;
    },
    setUser: (state,action) => {
      state.user = action.payload;
    },
    setAuthentificated: (state,action) => {
      state.authentificated = action.payload;
    },
    logout: (state) => {
      state.user = null;
      state.authentificated = false;
      setAuthToken(null);
      
    }
  }
});

export const { setToken,setUser, setAuthentificated, logout } = UserSlice.actions;
export const selectCurrentUser = (state) => state.user.user;
export const selectToken = (state) => state.user.token;
export const selectAuthentificated = (state) => state.user.authentificated;
export default UserSlice.reducer;
