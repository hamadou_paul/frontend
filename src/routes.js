import Produits from "./pages/Produits/Produits";
import { FaUsers } from "react-icons/fa";
import { BiCategory } from "react-icons/bi";
import Users from "./pages/Users/Users";

const routes = [  
  { 
    name: "Utilisateurs", 
    to: "/utilisateurs", 
    icon: FaUsers, 
    element: Users,
    code: "all",
  },
  { 
    name: "Produits", 
    to: "/produits", 
    icon: BiCategory , 
    element: Produits,
    code: "all",
  }
];


export default routes;